#imports
import csv


#check for save file, if none exists, create one. then, continue on.
def check_save_file():
    try:
        with open('save.txt', 'r') as f:
            print('Save file found, loading your game...')
            reader = csv.reader(f, delimiter=':')
            lines = list(reader)
            player = save_parser(lines)
            return(player)

    except IOError:
        with open('save.txt', 'w') as f:
            print('No save file found, creating a new character...')
            name = 'name'
            level = 1
            health = 10
            lines = [(name, level, health)]
            writer = csv.writer(f, delimiter=':')
            writer.writerows(lines)
            player = {
                'name': name,
                'level': level,
                'health': health,
            }
            return(player)

#parse the save file
def save_parser(lines):
    name = lines[0][0]
    level = lines[0][1]
    health = lines[0][2]
    player = {
        'name':name,
        'level': level,
        'health': health,
    }
    return(player)

#load save or start new game
def load_or_startnew():
    return
